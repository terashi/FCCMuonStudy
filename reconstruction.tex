\subsection{Momentum reconstruction}
The muon momentum is reconstructed by translating the azimuthal angle $\phi$ measured with the muon detector to the transverse momentum \pt using the relation:
\begin{equation}
\pt=0.3B\rho
\end{equation}
and 
\begin{equation}
\cos(\pi/2-\phi)=R/(2\rho)
\end{equation}
where $\rho$ is the curvature of the muon track and $B$ is the magnitude of the $B$-field, under the assumption of a constant $B$-field of 4~Tesla within the radius $R=6$~m. The muon track is reconstructed by a straight-line fit to the average hit positions in the four layers, for each of the barrel and endcap detectors, and extrapolating the fit to the point where the reconstructed muon exits from the $B$-field. Only hits created by the injected muons (identified with GEANT information) are used in the reconstruction. 
The azimuthal angle between the extrapolated line and the line connecting between the detector center and the point of the muon exit is used for the momentum measurement. The extrapolation used in this approach could introduce a bias to the measured momentum scale and this effect is accounted for by the calibration procedure described later. The momentum reconstruction scheme is illustrated in Fig.~\ref{fig:muon_reco}.

\begin{figure}[htbp]
  \centering
  \includegraphics[width=0.5\columnwidth]{figure/muon_reco_scheme.pdf}
  \caption{Muon momentum reconstruction scheme used in this analysis. The constant $B$-field of 4~Tesla in $z$ direction is assumed inside the solenoid. The average hit position per layer (orange circles) is used for a straight-line fit and extrapolation to measure the angle $\phi$ for each muon event.}
  \label{fig:muon_reco}
\end{figure}

\begin{figure}[htbp]
  \centering
  \begin{subfigure}{0.45\columnwidth}
    \centering
    \includegraphics[width=\columnwidth]{figure/fit_pt10GeV_vs_eta.pdf}
    \caption{$\pt=10$~GeV}
  \end{subfigure}
  \begin{subfigure}{0.45\columnwidth}
    \centering
    \includegraphics[width=\columnwidth]{figure/fit_pt100GeV_vs_eta.pdf}
    \caption{$\pt=100$~GeV}
  \end{subfigure}
  \begin{subfigure}{0.45\columnwidth}
    \centering
    \includegraphics[width=\columnwidth]{figure/fit_pt500GeV_vs_eta.pdf}
    \caption{$\pt=500$~GeV}
  \end{subfigure}
  \caption{Reconstructed muon \pt distributions for muons with the generator-level \pt at (a) 10, (b) 100 and (c) 500~GeV at different $\eta$ values.}
  \label{fig:mu_pt_vs_eta}
\end{figure}

\begin{figure}[htbp]
  \centering
  \includegraphics[height=0.5\columnwidth]{figure/fit_pt_resol_vs_eta.pdf}
  \caption{Relative \pt resolution for reconstructed muon as a function of muon $\eta$ for different generator-level \pt values between 10 and 1000~GeV.}
  \label{fig:mu_ptresol_vs_eta}
\end{figure}


Figure~\ref{fig:mu_pt_vs_eta} shows the reconstructed \pt distributions for the generator-level muons of $\pt=10$, 100 and 500~GeV injected at different $\eta$ positions from 0 to 1.8. The small offset of about 2-3~GeV with respect to the injected momentum of 10~GeV is attributed to the minimum-ionizing energy loss due to interaction with the material precedent of the muon detector. The relative \pt resolution of reconstructed muon as a function of muon $\eta$ is shown in Fig.~\ref{fig:mu_ptresol_vs_eta} for the \pt range between 10 and 1000~GeV.

\begin{figure}[htbp]
  \centering
  \includegraphics[height=0.5\columnwidth]{figure/mu_recoeff_vs_pt.pdf}
  \caption{Stand-alone muon reconstruction efficiency as a function of generator-level muon \pt at different $\eta$ values between 0.0 and 0.8.}
  \label{fig:mu_recoeff_vs_pt}
\end{figure}


Lowering the muon transverse momentum from 10~GeV, the reconstruction efficiency is expected to decrease significantly when the muon barely reaches the muon detector due to bending by the $B$-field.
Figure~\ref{fig:mu_recoeff_vs_pt} shows muon reconstruction efficiency as a function of generator-level muon \pt (\ptmutruth) between 3 and 10~GeV for different $\eta$ positions. The muons with $\pt>7.5$~GeV are reconstructed about more than 90\% of the time while those with $\pt<6.5$~GeV are hardly reconstructed at this $\eta$ range.



\subsection{Momentum calibration}
The measured momentum scale varies with the position where the angle of the muon track is measured with respect to the straight-line fit. To reduce the position dependence, a simple calibration procedure exploiting the correlation between the true muon energy and detector observables is adopted. The calibration is done by using either simulation or data, depending on how the correct momentum scale is obtained. For the simulation-based calibration, the generator-level muon energy is used as described below. For the data-based calibration, this could be switched to the muon momentum measured using inner tracker.

The detector observable could be, for example, the angle $\Delta\phi$ between the fit to the muon layer hits and the straight line drawn from the detector center to the innermost or outermost muon layer hits, or the transverse distance $\Delta L$ between the individual layer hits and the position where the straight line between the detector center and the outermost muon layer hits, as shown in Fig.~\ref{fig:muon_calib}.
 Figure~\ref{fig:calib_var_vs_pt}  shows two-dimensional correlation between the inverse of the muon \ptmutruth ranging between 10 and 100~GeV and the $\Delta\phi$ measured at the first and fourth layers or the $\Delta L$ measured at the second and third layers of the muon detector. Performing a second-order polynomial fit to the mean values as a function of $1/\ptmutruth$, the mapping between the detector observables and the true momentum scale can be extracted. In order to evaluate the resolution of reconstructed muon momentum, this mapping is applied to $\Delta\phi$ or $\Delta L$ values measured from a statistically independent set of simulation samples with the same \ptmutruth range. The resolution is defined here as a root-mean-square of the deviation from unity of the ratio of the inverse of the calibrated reconstructed \pt to the inverse of the \ptmutruth for each sample. Figure~\ref{fig:calib_ptresol_vs_pt} shows the result, indicating that a typical momentum resolution of about 10\% is obtained for the measurement of $\Delta\phi$ angles at the \pt range of $10-100$~GeV. The worse resolution for the $\Delta L$ measurement at the third layer is qualitatively understood to be due to limited lever-arm between the third and fourth layers. 


\begin{figure}[htbp]
  \centering
  \includegraphics[width=0.4\columnwidth]{figure/muon_calib.pdf}
  \caption{Detector observables (e.g, $\Delta\phi$ or $\Delta L$) that could be used to calibrate the muon momentum scale. The dashed lines connect the detector center and the positions of muon hits in the first or fourth layers, and are used together with the muon trajectory (solid curve) to measure the angle $\Delta\phi$.}
  \label{fig:muon_calib}
\end{figure}


\begin{figure}[htbp]
  \centering
  \begin{subfigure}{0.45\columnwidth}
    \centering
    \includegraphics[width=\columnwidth]{figure/calib_dphi_vs_pt.pdf}
    \caption{$\Delta\phi$ versus $1/\ptmutruth$}
  \end{subfigure}
  \begin{subfigure}{0.45\columnwidth}
    \centering
    \includegraphics[width=\columnwidth]{figure/calib_dl_vs_pt.pdf}
    \caption{$\Delta L$ versus $1/\ptmutruth$}
  \end{subfigure}
  \caption{(a) Two dimensional correlation between $\Delta\phi$ or (b) $\Delta L$ measured at the muon detector and the inverse of the generator-level muon \pt in the range between 10 and 100~GeV. Each curve on the distribution represent a second-order polynomial fit to extract the mapping between the observable and the true muon momentum used in the calibration.}
  \label{fig:calib_var_vs_pt}
\end{figure}

\begin{figure}[htbp]
  \centering
  \includegraphics[height=0.5\columnwidth]{figure/calib_ptresol_vs_pt.pdf}
  \caption{Reconstructed muon momentum resolution, defined as a root-mean-square of the deviation from unity of the ratio of the inverse of the calibrated reconstructed \pt to the inverse of the \ptmutruth, as a function of $1/\ptmutruth$ for the range between 10 and 100~GeV. The measurement is performed using the angles $\Delta\phi$ at the first and fourth layers or the transverse distance $\Delta L$ at the second and third layers of the muon detector.}
  \label{fig:calib_ptresol_vs_pt}
\end{figure}

