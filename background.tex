The primary interest of the muon detector is to identify and reconstruct muons produced in the decay of electroweak gauge bosons. However, it is important to evaluate the contribution from non-prompt muon sources, e.g, muons produced from the decay of heavy flavor hadrons, or hadronic background such as high-\pt particles punching through the region with less calorimeter material. Such contribution will have a direct impact on the event rate triggering on the activity in the muon detector.

\begin{figure}[htbp]
  \centering
  \begin{subfigure}{0.45\columnwidth}
    \centering
    \includegraphics[width=\columnwidth]{figure/jet_eta_muhits_pt10.pdf}
    \caption{$\hat{p}_{\text{T}}>10$~GeV}
  \end{subfigure}
  \begin{subfigure}{0.45\columnwidth}
    \centering
    \includegraphics[width=\columnwidth]{figure/jet_eta_muhits_pt100.pdf}
    \caption{$90<\hat{p}_{\text{T}}<110$~GeV}
  \end{subfigure}
  \begin{subfigure}{0.45\columnwidth}
    \centering
    \includegraphics[width=\columnwidth]{figure/jet_eta_muhits_pt1000.pdf}
    \caption{$900<\hat{p}_{\text{T}}<1100$~GeV}
  \end{subfigure}
  \caption{Jet $\eta$ distributions within $0<\eta<2.5$ for inclusive (black histograms) and HF (red histograms) jets in dijet events with $\hat{\pt}$ in the range (a) $\hat{p}_{\text{T}}>10$~GeV, (b) $90<\hat{p}_{\text{T}}<110$~GeV and (c) $900<\hat{p}_{\text{T}}<1100$~GeV. All the distributions are normalized to the integrated luminosity of 1~pb$^{-1}$. The solid histograms show the distributions for all jets while the dashed histograms show only those with simulation hits in the muon detector. The dotted histograms show the distributions for only those with simulation hits and generator-level muons inside the jet.}
  \label{fig:jet_eta_muhits}
\end{figure}


In order to assess those contribution in the current detector design, dijet events are produced using Pythia~8 and the activity in the muon detector is studied. Two samples of dijet events are generated in $2\to2$ hard scattering processes; the first sample with gluons and all flavor quarks as outgoing partons (referred to as inclusive jet sample) and the second one with only $b$- and $c$-quarks as outgoing partons (referred to as HF jet sample). For both samples at least one outgoing quark or gluon is required to have $\hat{p}_{\text{T}}>10$~GeV, $90<\hat{p}_{\text{T}}<110$~GeV or $900<\hat{p}_{\text{T}}<1100$~GeV to study jet \pt dependence. Jets are reconstructed at the generator level from all stable particles excluding neutrinos using the fastjet package, and any simulation-level hits in the muon detector are associated to jets with the requirement of $\Delta R<0.4$ around the jet axis. As seen in Fig.~\ref{fig:jet_eta_muhits}, about 2 (3)\%, 8 (10)\% and 30 (20)\% of inclusive (HF) jets have associated hits in the muon detector for dijets with $\hat{p}_{\text{T}}>10$~GeV, $90<\hat{p}_{\text{T}}<110$~GeV and $900<\hat{p}_{\text{T}}<1100$~GeV, respectively, 
with the relative fraction increasing from the barrel to the endcap region. The relative fraction becomes highest at $\eta$ around 1.7 presumably due to less material before the muon detector, as indicated in the muon energy loss (Fig.~\ref{fig:mu_ene_loss}) as well.
For dijets with $\hat{p}_{\text{T}}>10$~GeV ($90<\hat{p}_{\text{T}}<110$~GeV, $900<\hat{p}_{\text{T}}<1100$~GeV), about 13, (40, 12)\% of HF jets with muon detector hits contains a generator-level muon within $\Delta R=0.4$ around the jets while this fraction is only 2 (8, 5)\% for the inclusive jets. For $\sim1$~TeV jets, the contribution from real muons to muon detector hits is relatively reduced compared to the lower \pt jets of $\sim100$~GeV.

From Fig.~\ref{fig:jet_eta_muhits}, approximately 49M HF jets (per pb$^{-1}$) with $\hat{p}_{\text{T}}>10$~GeV and $|\eta|<2.5$ have hits in the muon detector, out of which 6.5M HF jets contain generator-level muons in them. Assuming the peak luminosity of $3 \times 10^{35}$~cm$^{-2}$s$^{-1}$ at the FCC-hh, this corresponds to a jet rate of approximately 15 (2.0)~MHz for all HF jets (HF jets with real muons) at $\hat{p}_{\text{T}}>10$~GeV.  
%Scaling the rate for the full HF jet spectrum of $\hat{p}_{\text{T}}>1$~GeV, this gives a total HF jet rate of 50 (3.5)~MHz. 
The inclusive and HF jets are studied for events with $1<\hat{p}_{\text{T}}<10$~GeV as well, however, the results are not considered in the rate estimation as the statistics of such low $\hat{p}_{\text{T}}$ jets with muon detector hits is quite limited and has large statistical fluctuation. The HF jet rate of 2.0~MHz will be the subject to isolation requirements investigated below.

The muons in jets are surrounded by other hadronic activity, so these muons are expected to be reduced significantly with some isolation requirement. Fig.~\ref{fig:mu_chgiso} shows \pt and relative "isolation" distributions of the generator-level muons found inside HF jets with $|\eta|<2$ in events with $90<\hat{p}_{\text{T}}<110$~GeV. Here the relative isolation is defined as the ratio of the scalar \pt sum of charged particles with $\pt>1$~GeV associated with HF jets within $\Delta R=0.4$ around the jet axis, excluding the muon itself, to the \pt of the muon. As expected, the muons inside HF jets have large associated activity, resulting in a significant reduction in rate if certain isolation condition is required. If the relative isolation is required to be less than 10\%, the muon contribution from HF jets is reduced to the level of 1\%.

The inclusive jet rates with muon detector hits are examined further for the barrel ($|\eta|<1.0$), inner endcap ($1.5<|\eta|<2.5$) and outer endcap ($1.0<|\eta|<1.5$) regions separately. For jets with $\hat{p}_{\text{T}}>10$~GeV, the average number of hits registered within $\Delta R<0.4$ of jets is translated to the number of hits per $\eta$-$\phi$ area, approximately 43 (44, 35, 34) with the statistical uncertainty of about 4 for the first (second, third, fourth) station in the barrel region, 34 (36, 36, 35) with the uncertainty of $\sim2$ in the inner endcap and 39 (35, 26, 28) with the uncertainty of 5 in the outer endcap regions. 
Convoluting them with the rates of jets with hits in each of the there detector components (Fig.~\ref{fig:jet_eta_muhits}), the hit rates per $\eta$-$\phi$ are estimated to be around a few GHz up to 10-20~GHz as shown in Table~\ref{tab:hit_rate}. 


\begin{figure}[htbp]
  \centering
  \begin{subfigure}{0.45\columnwidth}
    \centering
    \includegraphics[width=\columnwidth]{figure/mupt_muhits.pdf}
    \caption{Muon \pt in HF jets}
  \end{subfigure}
  \begin{subfigure}{0.45\columnwidth}
    \centering
    \includegraphics[width=\columnwidth]{figure/mu_chgiso.pdf}
    \caption{Charged particle isolation in HF jets}
  \end{subfigure}
  \caption{(a) Generator-level muon \pt distributions for all HF jets (solid histogram) and HF jets with the muon detector hits (shaded histogram) in dijet events with $90<\hat{p}_{\text{T}}<110$~GeV. The HF jets with $|\eta|<2$ are selected. (b) Relative isolation distribution calculated from charged particles within the HF jet, as described in the text.}
  \label{fig:mu_chgiso}
\end{figure}

\begin{table}
  \centering
  \caption{Rates of inclusive jets with muon detector hits and associated hits registered per $\eta$-$\phi$ area in the barrel and endcap muon detectors at an instantaneous luminosity of $3 \times 10^{35}$~cm$^{-2}$s$^{-1}$. Inclusive jets with $\hat{p}_{\text{T}}>10$~GeV are considered.}
  \label{tab:hit_rate}
  \begin{tabular}{l|c|cccccccc}
    \hline\noalign{\smallskip}
    Detector & $|\eta|$ & \multicolumn{4}{c}{Jet rate [MHz]} & \multicolumn{4}{c}{Hit rate per $\eta$-$\phi$ area [GHz]} \\
    \hspace{5mm} Station \# & & 1 & 2 & 3 & 4 & 1 & 2 & 3 & 4 \\
    \noalign{\smallskip}\hline\noalign{\smallskip}
    Barrel  & $<1.0$ & 110 & 103 & 92 & 103 & 4.7 & 4.5 & 3.3 & 3.5    \\
    Inner Endcap  & $1.5-2.5$ & 240 & 217 & 733 & 555 & 8.1 & 7.7 & 26.4 & 19.7    \\
    Outer Endcap & $1.0-1.5$ & 71 & 31 & 30 & 29 & 2.8 & 1.1 & 0.79 & 0.81    \\
    \noalign{\smallskip}\hline
  \end{tabular}
\end{table}
